﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Options;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Threading;

namespace AdventureMap.WebApp.Services.Chunks
{
    public class CacheChunkStore : IChunkStore
    {
        private const string MemoryCachePrefix = "MapChunk";

        private readonly ILogger<CacheChunkStore> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IOptions<MapOptions> _mapOptions;
        private readonly Dictionary<Point, Chunk> _spawnChunks;

        private CancellationTokenSource _resetCacheTokenSource;

        public CacheChunkStore(
            ILogger<CacheChunkStore> logger,
            IMemoryCache memoryCache,
            IOptions<MapOptions> mapOptions)
        {
            _resetCacheTokenSource = new CancellationTokenSource();
            _spawnChunks = new Dictionary<Point, Chunk>();
            _logger = logger;
            _memoryCache = memoryCache;
            _mapOptions = mapOptions;
        }

        public void Clear()
        {
            _logger.LogInformation("Clearing cache chunk store...");

            lock (_resetCacheTokenSource)
            {
                _spawnChunks.Clear();

                if (!_resetCacheTokenSource.IsCancellationRequested && _resetCacheTokenSource.Token.CanBeCanceled)
                {
                    _resetCacheTokenSource.Cancel();
                    _resetCacheTokenSource.Dispose();
                }

                _resetCacheTokenSource = new CancellationTokenSource();
            }

            _logger.LogInformation("Cache chunk store cleared.");
        }

        public Chunk Get(Point chunkPosition)
        {
            if (_spawnChunks.TryGetValue(chunkPosition, out var spawnChunk))
            {
                return spawnChunk;
            }

            return _memoryCache.Get<Chunk>(BuildCacheKey(chunkPosition));
        }

        public IReadOnlyDictionary<Point, Chunk> GetSpawnChunks()
        {
            return new ReadOnlyDictionary<Point, Chunk>(_spawnChunks);
        }


        public void Set(Chunk chunk)
        {
            var entryOptions = BuildEntryOptions(chunk.Type);

            _memoryCache.Set(BuildCacheKey(chunk.Position), chunk, entryOptions);

            if (chunk.Type == ChunkType.Spawn)
            {
                _spawnChunks[chunk.Position] = chunk;
            }
        }

        public bool TryGet(Point chunkPosition, out Chunk chunk)
        {
            return _spawnChunks.TryGetValue(chunkPosition, out chunk)
                || _memoryCache.TryGetValue(BuildCacheKey(chunkPosition), out chunk);
        }

        private MemoryCacheEntryOptions BuildEntryOptions(ChunkType chunkType)
        {
            return new MemoryCacheEntryOptions()
                .SetPriority(CacheItemPriority.Normal)
                .SetAbsoluteExpiration(chunkType == ChunkType.Spawn
                    ? DateTimeOffset.MaxValue.AddDays(-1) - DateTimeOffset.Now
                    : _mapOptions.Value.ChunkExpirationTime)
                .AddExpirationToken(new CancellationChangeToken(_resetCacheTokenSource.Token));
        }

        private string BuildCacheKey(Point chunkPosition)
        {
            return $"{MemoryCachePrefix}_{chunkPosition}";
        }
    }
}
