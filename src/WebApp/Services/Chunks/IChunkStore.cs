﻿using AdventureMap.WebApp.Models;
using System.Collections.Generic;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Chunks
{
    public interface IChunkStore
    {
        void Clear();

        Chunk Get(Point chunkPosition);

        IReadOnlyDictionary<Point, Chunk> GetSpawnChunks();

        void Set(Chunk chunk);

        bool TryGet(Point chunkPosition, out Chunk chunk);
    }
}
