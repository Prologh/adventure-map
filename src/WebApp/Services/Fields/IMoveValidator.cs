﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IMoveValidator
    {
        bool IsValid(Map map, Point startPosition, Size moveVector);

        bool IsValid(Map map, Point destinaionPosition);
    }
}
