﻿using AdventureMap.WebApp.Models;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultMoveValidator : IMoveValidator
    {
        private readonly IFieldWalkabilityValidator _fieldWalkabilityValidator;

        public DefaultMoveValidator(IFieldWalkabilityValidator fieldWalkabilityValidator)
        {
            _fieldWalkabilityValidator = fieldWalkabilityValidator;
        }

        public bool IsValid(Map map, Point startPosition, Size moveVector)
        {
            return IsValid(map, startPosition + moveVector);
        }

        public bool IsValid(Map map, Point destinaionPosition)
        {
            if (map is null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            return _fieldWalkabilityValidator.IsWalkable(map, destinaionPosition);
        }
    }
}
