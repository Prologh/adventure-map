﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Services.Chunks;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public class RandomStartingPostionProvider : IStartingPositionProvider
    {
        private readonly Random _random;
        private readonly IFieldWalkabilityValidator _fieldWalkabilityValidator;
        private readonly IChunkStore _chunkStore;

        public RandomStartingPostionProvider(
            IFieldWalkabilityValidator fieldWalkabilityValidator,
            IChunkStore chunkStore)
        {
            _random = new Random();
            _fieldWalkabilityValidator = fieldWalkabilityValidator;
            _chunkStore = chunkStore;
        }

        public Point GetStartingPosition(Map map)
        {
            if (map is null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            var spawnChunks = _chunkStore.GetSpawnChunks();
            var spawnFieldCount = spawnChunks.Count * map.ChunkSize * map.ChunkSize;
            var positionsBlocked = new Dictionary<Point, bool>(capacity: spawnFieldCount);

            do
            {
                var randomX = _random.Next(-map.SpawnSize.Width / 2, map.SpawnSize.Width / 2 + 1);
                var randomY = _random.Next(-map.SpawnSize.Height / 2, map.SpawnSize.Height / 2 + 1);
                var randomPoint = new Point(randomX, randomY);

                if (positionsBlocked.ContainsKey(randomPoint))
                {
                    continue;
                }

                if (!CheckIfInBounds(spawnChunks, map.ChunkSize, randomX, randomY))
                {
                    continue;
                }

                if (CheckIfNotBlocked(map, randomX, randomY))
                {
                    return randomPoint;
                }
                else
                {
                    positionsBlocked[randomPoint] = true;
                }

            } while (true);
        }

        private bool CheckIfNotBlocked(Map map, int x, int y)
        {
            if (!_fieldWalkabilityValidator.IsWalkable(map, x, y))
            {
                return false;
            }
            else if (!_fieldWalkabilityValidator.IsWalkable(map, x - 1, y))
            {
                return false;
            }
            else if (!_fieldWalkabilityValidator.IsWalkable(map, x + 1, y))
            {
                return false;
            }
            else if (!_fieldWalkabilityValidator.IsWalkable(map, x, y - 1))
            {
                return false;
            }
            else if (!_fieldWalkabilityValidator.IsWalkable(map, x, y + 1))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckIfInBounds(IReadOnlyDictionary<Point, Chunk> spawnChunks, int chunkSize, int fieldX, int fieldY)
        {
            var chunkPosition = new Point(
                (int)Math.Round(fieldX * 1.0d / chunkSize),
                (int)Math.Round(fieldY * 1.0d / chunkSize));

            return spawnChunks.ContainsKey(chunkPosition);
        }
    }
}
