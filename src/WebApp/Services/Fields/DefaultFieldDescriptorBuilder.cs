﻿using AdventureMap.WebApp.Models;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultFieldDescriptorBuilder : IFieldDescriptorBuilder
    {
        protected Color _color;
        protected string _iconFileName;
        protected bool _isWalkable;
        protected string _name;
        protected Range<double> _range;

        public FieldDescriptor Build()
        {
            return new FieldDescriptor
            {
                Color = _color,
                IconInfo = _iconFileName != null
                    ? new IconInfo
                    {
                        FilePath = _iconFileName,
                    }
                    : null,
                IsWalkable = _isWalkable,
                Name = _name,
                Range = _range,
            };
        }

        public IFieldDescriptorBuilder HasColor(Color color)
        {
            _color = color;

            return this;
        }

        public IFieldDescriptorBuilder HasIcon(string relativeFilePath)
        {
            if (string.IsNullOrWhiteSpace(relativeFilePath))
            {
                throw new ArgumentException(
                    $"{nameof(relativeFilePath)} cannot be null, empty or contain only whitespace characters.",
                    nameof(relativeFilePath));
            }

            _iconFileName = relativeFilePath;

            return this;
        }

        public IFieldDescriptorBuilder HasName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(
                    $"{nameof(name)} cannot be null, empty or contain only whitespace characters.",
                    nameof(name));
            }

            _name = name;

            return this;
        }

        public IFieldDescriptorBuilder HasValueRange(double range)
        {
            _range = new Range<double>(0, range);

            return this;
        }

        public IFieldDescriptorBuilder HasValueRange(double from, double to)
        {
            _range = new Range<double>(from, to);

            return this;
        }

        public IFieldDescriptorBuilder HasValueRange(Range<double> range)
        {
            _range = range;

            return this;
        }

        public IFieldDescriptorBuilder IsWalkable()
        {
            _isWalkable = true;

            return this;
        }
    }
}
