﻿using AdventureMap.WebApp.Models;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IMoveDirectionProvider
    {
        MoveDirection GetMoveDirection(MoveDirectionName moveDirectionName);
    }
}
