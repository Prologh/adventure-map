﻿using AdventureMap.WebApp.Models;
using System.Collections.Generic;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IFieldDescriptorProvider
    {
        IReadOnlyList<FieldDescriptor> GetAllFieldDescriptors();

        FieldDescriptor GetFieldDescriptor(double value);
    }
}
