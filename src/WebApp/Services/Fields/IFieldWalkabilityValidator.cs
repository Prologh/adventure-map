﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IFieldWalkabilityValidator
    {
        bool IsWalkable(Map map, Point destination);

        bool IsWalkable(Map map, int x, int y);
    }
}
