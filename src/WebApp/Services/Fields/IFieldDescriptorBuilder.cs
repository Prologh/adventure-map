﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IFieldDescriptorBuilder
    {
        FieldDescriptor Build();

        IFieldDescriptorBuilder HasColor(Color color);

        IFieldDescriptorBuilder HasIcon(string relativeFilePath);

        IFieldDescriptorBuilder HasName(string name);

        IFieldDescriptorBuilder HasValueRange(double range);

        IFieldDescriptorBuilder HasValueRange(double from, double to);

        IFieldDescriptorBuilder HasValueRange(Range<double> range);

        IFieldDescriptorBuilder IsWalkable();
    }
}
