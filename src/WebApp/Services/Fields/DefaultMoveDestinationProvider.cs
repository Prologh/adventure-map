﻿using AdventureMap.WebApp.Models;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultMoveDestinationProvider : IMoveDestinationProvider
    {
        private readonly IMoveDirectionProvider _moveDirectionProvider;

        public DefaultMoveDestinationProvider(IMoveDirectionProvider moveDirectionProvider)
        {
            _moveDirectionProvider = moveDirectionProvider;
        }

        public Point GetMoveDestination(Point startintPosition, int step, MoveDirectionName moveDirectionName)
        {
            if (step <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(step));
            }

            var moveDirection = _moveDirectionProvider.GetMoveDirection(moveDirectionName);

            return startintPosition + (moveDirection.Vector * step);
        }
    }
}
