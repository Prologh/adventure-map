﻿using AdventureMap.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultFieldDescriptorProvider : IFieldDescriptorProvider
    {
        private const double MinValue = -0.5f;

        private readonly List<FieldDescriptor> _fieldDescriptors;

        public DefaultFieldDescriptorProvider()
        {
            _fieldDescriptors = new List<FieldDescriptor>
            {
                new DefaultFieldDescriptorBuilder()
                    .HasName("Deep water")
                    .HasColor(Color.FromArgb(0, 69, 97))
                    .HasIcon("img/deep-water-waves-blue-24.png")
                    .HasValueRange(0.25d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Medium depth water")
                    .HasColor(Color.FromArgb(0, 123, 174))
                    .HasIcon("img/medium-water-waves-blue-24.png")
                    .HasValueRange(0.125d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Shallow water")
                    .HasColor(Color.FromArgb(170, 223, 255))
                    .HasIcon("img/shallow-water-waves-blue-24.png")
                    .HasValueRange(0.125d)
                    .IsWalkable()
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Beach sand")
                    .HasColor(Color.FromArgb(240, 218, 107))
                    .HasIcon("img/sand-gold-dots-24.png")
                    .HasValueRange(0.0625d)
                    .IsWalkable()
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Grass")
                    .HasColor(Color.FromArgb(96, 128, 56))
                    .HasIcon("img/grass-24.png")
                    .HasValueRange(0.25d)
                    .IsWalkable()
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Dense pine tree forest")
                    .HasColor(Color.FromArgb(79, 106, 47))
                    .HasIcon("img/pine-tree-24.png")
                    .HasValueRange(0.125d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Hill")
                    .HasColor(Color.FromArgb(155, 118, 83))
                    .HasValueRange(0.0625d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Boulder field")
                    .HasColor(Color.FromArgb(149, 148, 139))
                    .HasValueRange(0.0625d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Rocky mountain")
                    .HasColor(Color.FromArgb(85, 65, 36))
                    .HasValueRange(0.0625d)
                    .Build(),
                new DefaultFieldDescriptorBuilder()
                    .HasName("Snowy mountain top")
                    .HasColor(Color.FromArgb(255, 255, 255))
                    .HasValueRange(1)
                    .Build(),
            };
        }

        public IReadOnlyList<FieldDescriptor> GetAllFieldDescriptors()
        {
            return _fieldDescriptors.AsReadOnly();
        }

        public FieldDescriptor GetFieldDescriptor(double value)
        {
            var treshold = MinValue;

            foreach (var currentField in _fieldDescriptors)
            {
                treshold += Math.Abs(currentField.Range.To - currentField.Range.From);

                if (treshold >= value)
                {
                    return currentField;
                }
            }

            throw new InvalidOperationException($"No matching field found for value {value}.");
        }
    }
}
