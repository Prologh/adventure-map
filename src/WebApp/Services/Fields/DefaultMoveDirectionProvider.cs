﻿using AdventureMap.WebApp.Models;
using System;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultMoveDirectionProvider : IMoveDirectionProvider
    {
        public MoveDirection GetMoveDirection(MoveDirectionName moveDirectionName)
        {
            return moveDirectionName switch
            {
                MoveDirectionName.North => MoveDirection.North,
                MoveDirectionName.NorthWest => MoveDirection.NorthWest,
                MoveDirectionName.West => MoveDirection.West,
                MoveDirectionName.SouthWest => MoveDirection.SouthWest,
                MoveDirectionName.South => MoveDirection.South,
                MoveDirectionName.SouthEast => MoveDirection.SouthEast,
                MoveDirectionName.East => MoveDirection.East,
                MoveDirectionName.NorthEast => MoveDirection.NorthEast,
                _ => throw new NotSupportedException(),
            };
        }
    }
}
