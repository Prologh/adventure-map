﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IMoveDestinationProvider
    {
        Point GetMoveDestination(Point startingPosition, int step, MoveDirectionName moveDirectionName);
    }
}
