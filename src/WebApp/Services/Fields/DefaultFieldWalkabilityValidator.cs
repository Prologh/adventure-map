﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Services.Chunks;
using System;
using System.Drawing;
using System.Linq;

namespace AdventureMap.WebApp.Services.Fields
{
    public class DefaultFieldWalkabilityValidator : IFieldWalkabilityValidator
    {
        private readonly IFieldDescriptorProvider _fieldDescriptorProvider;
        private readonly IChunkStore _chunkStore;

        public DefaultFieldWalkabilityValidator(
            IFieldDescriptorProvider fieldDescriptorProvider,
            IChunkStore chunkStore)
        {
            _fieldDescriptorProvider = fieldDescriptorProvider;
            _chunkStore = chunkStore;
        }

        public bool IsWalkable(Map map, Point destination) => IsWalkable(map, destination.X, destination.Y);


        public bool IsWalkable(Map map, int x, int y)
        {
            if (map is null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            if (Math.Abs(x) > map.SpawnSize.Width / 2 || Math.Abs(y) > map.SpawnSize.Height / 2)
            {
                return false;
            }

            var fieldValue = GetFieldValueByFieldPosition(map.ChunkSize, x, y);
            var fieldDescriptor = _fieldDescriptorProvider.GetFieldDescriptor(fieldValue);

            return map.Players.Values.All(player => player.Position != new Point(x, y))
                && fieldDescriptor.IsWalkable;
        }

        private double GetFieldValueByFieldPosition(int chunkSize, int fieldX, int fieldY)
        {
            var chunk = GetChunkByFieldPosition(chunkSize, fieldX, fieldY);

            var localX = fieldX + -chunk.Position.X * chunkSize + chunkSize / 2;
            var localY = fieldY + -chunk.Position.Y * chunkSize + chunkSize / 2;

            return chunk.Fields[localX, localY];
        }

        private Chunk GetChunkByFieldPosition(int chunkSize, int fieldX, int fieldY)
        {
            return _chunkStore.Get(new Point(
                (int)Math.Round(fieldX * 1.0d / chunkSize),
                (int)Math.Round(fieldY * 1.0d / chunkSize)));
        }
    }
}
