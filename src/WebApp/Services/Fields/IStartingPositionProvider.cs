﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Fields
{
    public interface IStartingPositionProvider
    {
        Point GetStartingPosition(Map map);
    }
}
