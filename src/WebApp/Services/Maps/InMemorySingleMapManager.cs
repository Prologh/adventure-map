﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Services.Chunks;
using Microsoft.Extensions.Logging;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Maps
{
    public class InMemorySingleMapManager : IMapManager
    {
        private readonly IMapGenerator _mapGenerator;
        private readonly IChunkStore _chunkStore;
        private readonly ILogger<InMemorySingleMapManager> _logger;

        public InMemorySingleMapManager(
            IMapGenerator mapGenerator,
            IChunkStore chunkStore,
            ILogger<InMemorySingleMapManager> logger)
        {
            _mapGenerator = mapGenerator;
            _chunkStore = chunkStore;
            _logger = logger;
        }

        public bool HasGeneratedMap => CurrentMap != null;

        public Map CurrentMap { get; private set; }

        public Chunk GenerateMapChunk(Point chunkPosition)
        {
            if (!HasGeneratedMap)
            {
                throw new InvalidOperationException("Cannot generate map chunk for not yet generated map.");
            }

            var chunk = _mapGenerator.GenerateMapChunk(CurrentMap.Seed, CurrentMap.ChunkSize, ChunkType.Remote, chunkPosition);

            _chunkStore.Set(chunk);

            return chunk;
        }

        public Map GenerateMap(int columns, int rows)
        {
            _logger.LogInformation("Generating initial map with {0} columns and {1} rows.", columns, rows);

            var seed = new Random().Next();

            var spawnChunks = _mapGenerator.GenerateSpawnChunks(
                seed,
                columns,
                rows,
                Map.DefaultChunkSideLength);

            CurrentMap = _mapGenerator.GenerateMap(
                seed,
                columns,
                rows,
                Map.DefaultChunkSideLength,
                spawnChunks.Count);

            foreach (var keyValuePair in spawnChunks)
            {
                _chunkStore.Set(keyValuePair.Value);
            }

            _logger.LogInformation("Initial map generated.");

            return CurrentMap;
        }

        public Map RegenerateMap()
        {
            if (!HasGeneratedMap)
            {
                throw new InvalidOperationException("Cannot regenerate not yet generated map.");
            }

            _logger.LogInformation("Regenerating existing map.");

            var size = CurrentMap.SpawnSize;

            RemoveMap();

            var regeneratedMap = GenerateMap(size.Width, size.Height);

            _logger.LogInformation("Map regenerated.");

            return regeneratedMap;
        }

        public void RemoveMap()
        {
            if (!HasGeneratedMap)
            {
                throw new InvalidOperationException("Cannot remove not yet generated map.");
            }

            _logger.LogInformation("Removing map.");

            CurrentMap = null;

            _chunkStore.Clear();

            _logger.LogInformation("Current map removed.");
        }

        public bool TryGetMapChunk(Point chunkPosition, out Chunk chunk)
        {
            return _chunkStore.TryGet(chunkPosition, out chunk);
        }
    }
}
