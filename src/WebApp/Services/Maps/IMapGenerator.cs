﻿using AdventureMap.WebApp.Models;
using System.Collections.Generic;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Maps
{
    public interface IMapGenerator
    {
        Map GenerateMap(int seed, int columns, int rows, int chunkSize, int spawnChunksCount);

        IDictionary<Point, Chunk> GenerateSpawnChunks(int seed, int columns, int rows, int chunkSize);

        Chunk GenerateMapChunk(int seed, int chunkSize, ChunkType chunkType, Point chunkPosition);
    }
}
