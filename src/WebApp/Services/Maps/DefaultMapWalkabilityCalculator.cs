﻿//using AdventureMap.WebApp.Models;
//using AdventureMap.WebApp.Services.Fields;
//using System;

//namespace AdventureMap.WebApp.Services.Maps
//{
//    public class DefaultMapWalkabilityCalculator : IMapWalkablilityCalculator
//    {
//        private readonly IFieldWalkabilityValidator _fieldWalkabilityValidator;

//        public DefaultMapWalkabilityCalculator(IFieldWalkabilityValidator fieldWalkabilityValidator)
//        {
//            _fieldWalkabilityValidator = fieldWalkabilityValidator;
//        }

//        public double CalculateMapWalkabilityRatio(Map map)
//        {
//            if (map is null)
//            {
//                throw new ArgumentNullException(nameof(map));
//            }

//            var walkableFieldsCount = 0;

//            foreach (var chunk in map.Chunks)
//            {
//                for (int x = 0; x < map.ChunkSize; x++)
//                {
//                    for (int y = 0; y < map.ChunkSize; y++)
//                    {
//                        if (_fieldWalkabilityValidator.IsWalkable(map, x, y))
//                        {
//                            walkableFieldsCount++;
//                        }
//                    }
//                }
//            }

//            return walkableFieldsCount * 1.0d / (map.Chunks.Count * map.ChunkSize * map.ChunkSize);
//        }
//    }
//}
