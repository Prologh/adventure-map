﻿using AdventureMap.WebApp.Models;

namespace AdventureMap.WebApp.Services.Maps
{
    public interface IMapWalkablilityCalculator
    {
        double CalculateMapWalkabilityRatio(Map map);
    }
}
