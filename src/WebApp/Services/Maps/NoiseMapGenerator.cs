﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Services.Fields;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace AdventureMap.WebApp.Services.Maps
{
    public class NoiseMapGenerator : IMapGenerator
    {
        private readonly ILogger<NoiseMapGenerator> _logger;
        private readonly IFieldDescriptorProvider _fieldDescriptorProvider;

        public NoiseMapGenerator(
            ILogger<NoiseMapGenerator> logger,
            IFieldDescriptorProvider fieldDescriptorProvider)
        {
            _logger = logger;
            _fieldDescriptorProvider = fieldDescriptorProvider;
        }

        public Map GenerateMap(int seed, int columns, int rows, int chunkSize, int spawnChunksCount)
        {
            if (chunkSize % 2 != 1)
            {
                throw new ArgumentException(
                    $"{nameof(chunkSize)} must be an uneven number.",
                    nameof(chunkSize));
            }

            return new Map(seed, columns, rows, chunkSize, _fieldDescriptorProvider.GetAllFieldDescriptors());
        }

        public IDictionary<Point, Chunk> GenerateSpawnChunks(int seed, int rows, int columns, int chunkSize)
        {
            if (chunkSize % 2 != 1)
            {
                throw new ArgumentException(
                    $"{nameof(chunkSize)} must be an uneven number.",
                    nameof(chunkSize));
            }

            var stopwatch = new Stopwatch();
            var fastNoise = new FastNoise(seed);
            var greaterLength = new[] { columns, rows }.Max() + chunkSize;
            var chunksQuarterSideLength = (int)Math.Ceiling(greaterLength * 1.0d / 2 / chunkSize);
            var chunks = new Dictionary<Point, Chunk>();

            _logger.LogInformation(
                "Starting spawn chunks generation. Seed = {0}, chunk size = {1}",
                seed,
                chunkSize);
            stopwatch.Start();

            for (int chunkX = -chunksQuarterSideLength; chunkX <= chunksQuarterSideLength; chunkX++)
            {
                for (int chunkY = -chunksQuarterSideLength; chunkY <= chunksQuarterSideLength; chunkY++)
                {
                    var chunk = GenerateMapChunk(fastNoise, chunkSize, ChunkType.Spawn, new Point(chunkX, chunkY));

                    chunks[chunk.Position] = chunk;
                }
            }

            stopwatch.Stop();
            _logger.LogInformation(
                "Noise map generated in {0} ms.",
                stopwatch.Elapsed.TotalMilliseconds);

            return chunks;
        }

        public Chunk GenerateMapChunk(int seed, int chunkSize, ChunkType chunkType, Point chunkPosition)
        {
            var fastNoise = new FastNoise(seed);

            return GenerateMapChunk(fastNoise, chunkSize, chunkType, chunkPosition);
        }

        private Chunk GenerateMapChunk(FastNoise fastNoise, int chunkSize, ChunkType chunkType, Point chunkPosition)
        {
            var chunkFieldArray = new double[chunkSize, chunkSize];

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int localX = -chunkSize / 2; localX <= chunkSize / 2; localX++)
            {
                for (int localY = -chunkSize / 2; localY <= chunkSize / 2; localY++)
                {
                    var globalX = localX + chunkPosition.X * chunkSize;
                    var globalY = localY + chunkPosition.Y * chunkSize;

                    var noiseValue = fastNoise.GetPerlinFractal(globalX, globalY);

                    chunkFieldArray[localX + chunkSize / 2, localY + chunkSize / 2] = noiseValue;
                }
            }

            stopwatch.Stop();

            return new Chunk(chunkSize, chunkPosition, chunkType, chunkFieldArray);
        }
    }
}
