﻿using AdventureMap.WebApp.Models;
using System.Drawing;

namespace AdventureMap.WebApp.Services.Maps
{
    public interface IMapManager
    {
        bool HasGeneratedMap { get; }

        Map CurrentMap { get; }

        Map GenerateMap(int columns, int rows);

        Chunk GenerateMapChunk(Point chunkPosition);

        Map RegenerateMap();

        void RemoveMap();

        bool TryGetMapChunk(Point chunkPosition, out Chunk chunk);
    }
}
