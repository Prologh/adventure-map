﻿/// <binding Clean='clean' ProjectOpened='watch' />
'use strict';

// Load plugins
var gulp = require('gulp');
var gulpAutoprefixer = require('gulp-autoprefixer');
var gulpCssMin = require('gulp-cssmin');
var gulpRename = require('gulp-rename');
var gulpSass = require('gulp-sass');
var gulpTerser = require('gulp-terser');
var nodeSass = require('node-sass');
var rimraf = require('rimraf');

gulpSass.compiler = nodeSass;

const mainCssFileName = 'site';
const cappedMapJsFileName = 'CappedMap';
const mapJsFileName = 'map';
const mainSassFileName = 'site';
const mainBootstrapFileName = "bootstrap";

var paths = {
    nodeModules: './node_modules/',
    webroot: './wwwroot/'
};

paths.cssDir = paths.webroot + 'css/';
paths.jsDir = paths.webroot + 'js/';
paths.libDir = paths.webroot + 'lib/';
paths.scssDir = paths.webroot + 'scss/';
paths.bootstrapDir = paths.nodeModules + "bootstrap/";

paths.jsFiles = paths.jsDir + '*.js';
paths.minJsFilesExcluded = '!' + paths.jsDir + '*.min.js';
paths.scssFiles = paths.scssDir + '**/*.scss';

paths.mainCss = paths.cssDir + mainCssFileName + '.css';
paths.mainCssBootstrap = paths.cssDir + mainBootstrapFileName + '.css';
paths.cappedMapJs = paths.jsDir + cappedMapJsFileName + '.js';
paths.mapJs = paths.jsDir + mapJsFileName + '.js';
paths.mainMinCss = paths.cssDir + mainCssFileName + '.min.css';
paths.cappedMapMinJs = paths.jsDir + cappedMapJsFileName + '.min.js';
paths.mapMinJs = paths.jsDir + mapJsFileName + '.min.js';
paths.mainSass = paths.scssDir + mainSassFileName + '.scss';
paths.mainSassBootstrap = paths.bootstrapDir + 'scss/' + mainBootstrapFileName + '.scss';



/* ### Clean ### */

// Clean output files
gulp.task('clean:cssDir', done => rimraf(paths.cssDir, done));
gulp.task('clean:libDir', done => rimraf(paths.libDir, done));
gulp.task('clean:mapMinJs', done => rimraf(paths.mapMinJs, done));
gulp.task('clean:cappedMapMinJs', done => rimraf(paths.cappedMapMinJs, done));
gulp.task('clean', gulp.series(['clean:cssDir', 'clean:libDir', 'clean:mapMinJs', 'clean:cappedMapMinJs']));



/* ### Compile ### */

// Compile SCSS files to CSS
gulp.task('compile:scss:site', () => {
    return gulp.src(paths.mainSass)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer())
        .pipe(gulp.dest(paths.cssDir));
});

gulp.task('compile:scss:bootstrap', () => {
    return gulp.src(paths.mainSassBootstrap)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer())
        .pipe(gulpRename({ basename: 'bootstrap' }))
        .pipe(gulp.dest(paths.cssDir));
});

// SCSS compile task
gulp.task('compile:scss', gulp.series(['compile:scss:site', 'compile:scss:bootstrap']));

// Global compile task
gulp.task('compile', gulp.series(['compile:scss']));




/* ### Copy ### */

// Copy Font Awesome CSS files
gulp.task('copy:css:font-awesome:css', () => {
    return gulp.src(paths.nodeModules + '@fortawesome/fontawesome-free/css/*.css')
        .pipe(gulpRename({ dirname: paths.libDir + '/font-awesome/css/' }))
        .pipe(gulp.dest('.'));
});

// Copy Font Awesome web fonts files
gulp.task('copy:css:font-awesome:webfonts', () => {
    return gulp.src(paths.nodeModules + '@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulpRename({ dirname: paths.libDir + '/font-awesome/webfonts/' }))
        .pipe(gulp.dest('.'));
});

// Copy Font Awesome icon pack
gulp.task('copy:css:font-awesome', gulp.series(['copy:css:font-awesome:css', 'copy:css:font-awesome:webfonts']));

// Copy CSS libraries
gulp.task('copy:css', gulp.series(['copy:css:font-awesome']));

// Copy Bootstrap files
gulp.task('copy:js:bootstrap', () => {
    return gulp.src(paths.nodeModules + 'bootstrap/dist/js/bootstrap*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/bootstrap/' }))
        .pipe(gulp.dest('.'));
});

// Copy jQuery files
gulp.task('copy:js:jquery', () => {
    return gulp.src(paths.nodeModules + 'jquery/dist/jquery*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery/' }))
        .pipe(gulp.dest('.'));
});

// Copy jQuery Validate files
gulp.task('copy:js:jquery-validate', () => {
    return gulp.src(paths.nodeModules + 'jquery-validation/dist/jquery.validate*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery-validate/' }))
        .pipe(gulp.dest('.'));
});

// Copy jQuery Validate Unobtrusive files
gulp.task('copy:js:jquery-validate-unobtrusive', () => {
    return gulp.src(paths.nodeModules + 'jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery-validate-unobtrusive/' }))
        .pipe(gulp.dest('.'));
});

// Copy Bootstrap files
gulp.task('copy:js:signalr', () => {
    return gulp.src(paths.nodeModules + '@aspnet/signalr/dist/browser/*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/signalr/' }))
        .pipe(gulp.dest('.'));
});

// Copy JavaScript libraries
gulp.task('copy:js', gulp.series([
    'copy:js:bootstrap',
    'copy:js:jquery',
    'copy:js:jquery-validate',
    'copy:js:jquery-validate-unobtrusive',
    'copy:js:signalr'
]));

// Global Concat task
gulp.task('copy', gulp.series(['copy:css', 'copy:js']));



/* ### Minify ### */

// Minify CSS files
gulp.task("minify:css:site", () => {
    return gulp.src(paths.mainCss)
        .pipe(gulpCssMin())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.cssDir));
});

gulp.task("minify:css:bootstrap", () => {
    return gulp.src(paths.mainCssBootstrap)
        .pipe(gulpCssMin())
        .pipe(gulpRename({ basename: 'bootstrap', suffix: '.min' }))
        .pipe(gulp.dest(paths.cssDir));
});

// CSS minify task
gulp.task('minify:css', gulp.series(['minify:css:site', 'minify:css:bootstrap']));

gulp.task("minify:js:cappedMap", () => {
    return gulp.src(paths.cappedMapJs)
        .pipe(gulpTerser())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.jsDir));
});

gulp.task("minify:js:map", () => {
    return gulp.src(paths.mapJs)
        .pipe(gulpTerser())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.jsDir));
});

// JS minify task
gulp.task('minify:js', gulp.series(['minify:js:cappedMap', 'minify:js:map']));

// Global minify task
gulp.task('minify', gulp.series(['minify:js', 'minify:css']));



/* ### Watch ### */

// Watch scss files
gulp.task('watch:scss', () => {
    return gulp.watch(paths.scssFiles, gulp.series(['compile', 'minify:css']));
});

// Watch JavaScript files
gulp.task('watch:js', () => {
    gulp.watch([paths.jsFiles, paths.minJsFilesExcluded], gulp.series(['minify:js']));
});

// Global watch task
gulp.task('watch', gulp.parallel(['watch:scss', 'watch:js']));



/* ### Default ### */

gulp.task('default', gulp.series(['compile', 'copy', 'minify']));
