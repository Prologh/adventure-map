﻿$(document).ready(function () {

    const canvas = $('#mapCanvas')[0];
    const context = canvas.getContext("2d");
    const columns = canvas.getAttribute('data-columns');
    const rows = canvas.getAttribute('data-rows');
    const fieldSize = canvas.getAttribute('data-field-size');
    const columnWidth = canvas.width / columns;
    const rowHeight = canvas.height / rows;
    const frameDelay = 32;
    const $messageInput = $('#message');

    var map = null;
    var cappedChunksMap = new CappedMap(100);
    var keyboardEnabled = true;
    var chatVisible = false;
    var currentPlayerName = 'player' + Math.floor(Math.random() * 1000);
    var currentPlayerColor = getRandomColor();;
    var playersDict = {};
    var keyMap = {};
    var fieldImages = {};
    var ping = {};

    window.addEventListener('keyup',
        (e) => {
            keyMap[e.key] = false;
        },
        false);

    window.addEventListener('keydown',
        (e) => {
            keyMap[e.key] = true;
        },
        false);

    $('#regenerateMap').on('click', () => {
        regenerateMap();
    });

    $('#updatePlayerInfoButton').on('click', () => {
        var playerName = $('#playerNameInput').val();
        var playerColor = $('#playerColorInput').val()

        if (playerName && playerColor) {
            updatePlayerInfo(playerName, playerColor);
        }
    });

    // Disable keyboard detection when any modal is opening
    $('.modal').on('show.bs.modal', () => {
        keyboardEnabled = false;
    });

    // Enable keyboard detection when any modal is closed
    $('.modal').on('hidden.bs.modal', () => {
        keyboardEnabled = true;
    });

    // Set initial focus to message input box when modal is showing.
    $('#chat').on('shown.bs.modal', () => {
        scrollToTheDiscussionBottom();
        $('#message').focus();
        chatVisible = true;
        clearChatMessageNotifications();
    });

    $('#chat').on('hide.bs.modal', () => {
        chatVisible = false;
    });

    // Send message on send button click.
    $('#sendmessage').on('click', (event) => {
        sendMessage(event);
    });

    // Detect enter hit as message send trigger.
    $('#message').on('keypress', (event) => {
        if (event.which === 13) {
            sendMessage(event);
        }
    });

    var connection = new signalR.HubConnectionBuilder()
        .withUrl('/hubs/map')
        .build();

    connection.on('addPlayer', (newPlayer) => {

        playersDict[newPlayer.name] = newPlayer;

        if (newPlayer.name === currentPlayerName) {
            let miliseconds = performance.now() - ping['initial'];
            delete ping['initial'];

            loadMissingMapChunks(newPlayer.position);

            displayCurrentPlayerCoordinates();
            displayServerPing(miliseconds);
        }

        if (isInVisibleMapBounds(newPlayer.position)) {
            displayPlayerField(newPlayer);
        }

        displayPlayersCount();
        displayPlayerList();
    });

    connection.on('movePlayer', (name, startingX, startingY, destinationX, destinationY) => {
        if (name === currentPlayerName) {
            let pingKey = `${startingX}, ${startingY}`;
            let miliseconds = performance.now() - ping[pingKey];
            delete ping[pingKey];

            displayServerPing(miliseconds);
        } else {
            var player = playersDict[name];

            player.position = {
                X: destinationX,
                Y: destinationY
            };
            playersDict[name] = player;

            var startingPosition = {
                X: startingX,
                Y: startingY,
            };

            if (isInVisibleMapBounds(startingPosition)
                || isInVisibleMapBounds(player.position)) {
                clearCanvas();
                displayMapFields();
                displayPlayerFields();
            }
        }
    });

    connection.on('updatePlayerInfo', (oldPlayerName, playerInfo) => {
        delete playersDict[oldPlayerName];
        playersDict[playerInfo.name] = playerInfo;

        var $li = $('#playerListContainer #' + oldPlayerName);
        $li.attr('id', playerInfo.name);
        $li.text(playerInfo.name);

        if (oldPlayerName === currentPlayerName) {
            currentPlayerName = playerInfo.name;
            currentPlayerColor = playerInfo.color;

            var $li = $('#playerListContainer #' + oldPlayerName);
            $li.attr('id', currentPlayerName);
            $li.text(currentPlayerName);

            displayCurrentPlayerInfo();

            clearCanvas();
            displayMapFields();
            displayPlayerFields();
        }
        else {
            if (isInVisibleMapBounds(playerInfo.position)) {
                clearCanvas();
                displayMapFields();
                displayPlayerFields();
            }
        }
    });

    connection.on('getMapChunks', (chunks) => {
        $(chunks).each((i, chunk) => {
            let chunkPosition = chunk.position.X + ', ' + chunk.position.Y;
            cappedChunksMap.add(chunkPosition, chunk);
        });

        clearCanvas();
        displayMapFields();
        displayPlayerFields();
        displayLoadedChunksCount();
        displayLoadedFieldsCount();
    });

    connection.on('getMapInfo', (mapInfo) => {
        map = mapInfo;
        addExistingPlayers(mapInfo.players);

        //console.log("map columns: " + columns);
        //console.log("map rows: " + rows);
        //console.log("canvas width: " + canvas.width + "px");
        //console.log("canvas height: " + canvas.height + "px");
        //console.log("canvas field width: " + columnWidth + "px");
        //console.log("canvas field height: " + rowHeight + "px");

        displayCurrentPlayerInfo();
        displayMapInfo(map);
    });

    connection.on('regenerateMap', () => {
        keyboardEnabled = false;
        map = null;
        cappedChunksMap = new CappedMap(cappedChunksMap.getCap());
        playersDict = {};

        connection.invoke('getMapInfo', columns, rows);

        ping['initial'] = performance.now();

        connection.invoke('addPlayer', currentPlayerName, currentPlayerColor);

        keyboardEnabled = true;
    });

    connection.on('removePlayer', (player) => {
        $('#playerListContainer #' + player.name).remove();

        // Remove player from players dictionary.
        delete playersDict[player.name];

        // Redraw whole map.
        clearCanvas();
        displayMapFields();
        displayPlayerFields();
        displayPlayersCount();
        displayPlayerList();
    });

    connection.on('sendMessage', (author, message) => {
        notifyMessage();
        displayMessage(author, message);
    });

    connection.start()
        .then(() => {
            connection.invoke('getMapInfo', columns, rows);

            ping['initial'] = performance.now();

            connection.invoke('addPlayer', currentPlayerName, currentPlayerColor);
        })
        .catch(error => {
            console.error(error.message);
        });

    function addExistingPlayers(playersData) {
        $.each(playersData, (key, player) => {
            playersDict[player.name] = player;
        });
    }

    function checkIfMoveIsValid(destination) {
        var chunk = getChunk(destination.X, destination.Y);

        if (!chunk) {
            return false;
        }

        let chunkX = destination.X + -chunk.position.X * map.chunkSize + Math.floor(map.chunkSize / 2);
        let chunkY = destination.Y + -chunk.position.Y * map.chunkSize + Math.floor(map.chunkSize / 2);

        let fieldValue = chunk.fields[chunkX][chunkY];
        let fieldDescriptor = getFieldDescriptor(fieldValue);

        if (!fieldDescriptor.isWalkable) {
            return false;
        }

        if (isOccupiedByPlayer(destination.X, destination.Y)) {
            return false;
        }

        return true;
    }

    function clearCanvas() {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    function clearChatMessageNotifications() {
        $('#chatMessageNotification').hide();
        $('#chatMessageNotification').text(0);
        $('#chatMessageNotification').removeClass('bounce');
    }

    function displayCurrentPlayerCoordinates() {
        let currentPlayerPosition = playersDict[currentPlayerName].position;
        $('#playerPositionX').text(currentPlayerPosition.X);
        $('#playerPositionY').text(currentPlayerPosition.Y);
    }

    function displayCurrentPlayerInfo() {
        $('.currentPlayerName:not(input)').text(currentPlayerName);
        $('input.currentPlayerName').val(currentPlayerName);

        $('.currentPlayerColor:not(input)').text(currentPlayerColor);
        $('input.currentPlayerColor').val(currentPlayerColor);
    }

    function displayFieldImage(canvasX, canvasY, fieldDescriptor) {
        if (fieldDescriptor.iconInfo !== null) {
            let image = fieldImages[fieldDescriptor.name];

            if (!image) {
                image = new Image();
                image.onload = () => {
                    fieldImages[fieldDescriptor.name] = image;
                    drawImage(
                        image,
                        canvasX * columnWidth,
                        canvasY * rowHeight,
                        columnWidth,
                        rowHeight);
                }
                image.src = window.origin + '/' + fieldDescriptor.iconInfo.filePath;
            } else {
                drawImage(
                    image,
                    canvasX * columnWidth,
                    canvasY * rowHeight,
                    columnWidth,
                    rowHeight);
            }
        }
    }

    function displayFrameRenderTime(miliseconds) {
        let textColor;

        if (miliseconds > 16) {
            textColor = 'red';
        }
        else if (miliseconds > 10) {
            textColor = 'orange';
        } else if (miliseconds > 4) {
            textColor = 'inherit';
        } else {
            textColor = 'green';
        }

        $('#renderedIn').text(miliseconds.toFixed(3));
        $('#renderedIn').css('color', textColor);
    }

    function displayLoadedChunksCount() {
        $('#chunksLoaded').text(cappedChunksMap.getCount());
    }

    function displayLoadedFieldsCount() {
        $('#fieldsLoaded').text(cappedChunksMap.getCount() * map.chunkSize * map.chunkSize);
    }

    function displayMapFields() {
        let missingChunks = false;
        let perfStart = performance.now();
        let player = playersDict[currentPlayerName];
        let currentPosition = player.position;

        for (var canvasX = 0; canvasX < columns; canvasX++) {
            let mapX = currentPosition.X + canvasX - Math.floor(columns / 2);

            for (var canvasY = rows - 1; canvasY >= 0; canvasY--) {
                let mapY = currentPosition.Y + Math.floor(rows / 2) - canvasY;
                let chunk = getChunk(mapX, mapY);

                if (!chunk) {
                    missingChunks = true;
                    continue;
                }

                displayMapField(
                    chunk,
                    canvasX,
                    canvasY,
                    mapX,
                    mapY);

                mapY++;
            }

            mapX++;
        }

        var perfStop = performance.now();

        displayFrameRenderTime(perfStop - perfStart);

        if (missingChunks) {
            setTimeout(() => loadMissingMapChunks(currentPosition), 0);
        }
    }

    function displayMapField(chunk, canvasX, canvasY, mapX, mapY) {
        let chunkX = mapX + -chunk.position.X * map.chunkSize + Math.floor(map.chunkSize / 2);
        let chunkY = mapY + -chunk.position.Y * map.chunkSize + Math.floor(map.chunkSize / 2);

        let fieldValue = chunk.fields[chunkX][chunkY];
        let fieldDescriptor = getFieldDescriptor(fieldValue);

        drawRectangle(
            canvasX * columnWidth,
            canvasY * rowHeight,
            columnWidth,
            rowHeight,
            fieldDescriptor.color);

        displayFieldImage(canvasX, canvasY, fieldDescriptor);
    }

    function displayMapInfo(map) {
        $('#mapSeed').text(map.seed);
    }

    function displayMessage(author, message) {
        var messageElement = document.createElement('div');
        $(messageElement).addClass('message');
        var messageBody = document.createElement('div');
        $(messageBody).addClass('message-body');
        if (author === currentPlayerName) {
            $(messageElement).addClass('text-right');
            $(messageBody).addClass('bg-primary');
            $(messageBody).addClass('text-light');
        }
        messageBody.innerHTML = '<strong>' + author + '</strong>:&nbsp;&nbsp;' + message;
        messageElement.appendChild(messageBody);
        $('#discussion').append(messageElement);

        scrollToTheDiscussionBottom();
    }

    function displayPlayersCount() {
        $('.playersCountBadge').text(Object.keys(playersDict).length);
    }

    function displayPlayerFields() {
        if (!playersDict[currentPlayerName]) {
            return;
        }

        $.each(playersDict, (key, player) => {
            if (isInVisibleMapBounds(player.position)) {
                displayPlayerField(player);
            }
        });
    }

    function displayPlayerField(player) {
        let mapX = player.position.X;
        let mapY = player.position.Y;
        let currentPlayerPosition = playersDict[currentPlayerName].position;
        let canvasX = mapX - currentPlayerPosition.X + Math.floor(columns / 2);
        let canvasY = currentPlayerPosition.Y - mapY + Math.floor(rows / 2);

        // Draw player field black background
        drawRectangle(
            canvasX * columnWidth,
            canvasY * rowHeight,
            columnWidth,
            rowHeight,
            'black');

        // Draw actual player field
        drawRectangle(
            canvasX * columnWidth + (fieldSize / 10),
            canvasY * rowHeight + (fieldSize / 10),
            columnWidth - (fieldSize / 5),
            rowHeight - (fieldSize / 5),
            player.color);

        let maxNameLength = columnWidth * 4;

        let playerNameLength = context.measureText(player.name).width;
        let playerName = player.name;

        while (playerNameLength > maxNameLength) {
            playerName = playerName.substring(0, playerName.length - 1);
            playerNameLength = context.measureText(playerName).width;
        }

        // Draw half-transparent gray background for player name text
        drawRectangle(
            (canvasX) * columnWidth + columnWidth / 2 - playerNameLength / 2,
            (canvasY - 1) * rowHeight,
            playerNameLength,
            rowHeight,
            'rgba(255, 255, 255, 0.7)');

        // Draw black player name text
        context.font = "bold 14px Arial";
        context.fillStyle = "black";
        context.textAlign = "center";
        context.fillText(playerName, canvasX * columnWidth + columnWidth / 2, canvasY * rowHeight - (fieldSize / 5));
    }

    function displayPlayerList() {
        $('#playerListContainer').empty();

        $.each(playersDict, (key, player) => {
            $('#playerListContainer').append('<li id="' + player.name + '">' + player.name + '</li>');
        });
    }

    function displayServerPing(miliseconds) {
        if (!miliseconds || isNaN(miliseconds)) {
            return;
        }

        let textColor;

        if (miliseconds > 30) {
            textColor = 'red';
        }
        else if (miliseconds > 20) {
            textColor = 'orange';
        } else if (miliseconds > 10) {
            textColor = 'inherit';
        } else {
            textColor = 'green';
        }

        $('#ping').text(miliseconds.toFixed(3));
        $('#ping').css('color', textColor);
    }

    function drawImage(image, x, y, w, h) {
        context.drawImage(image, x, y, w, h);
    }

    function drawRectangle(x, y, width, height, color) {
        context.beginPath();
        context.rect(x, y, width, height);
        context.fillStyle = color;
        context.fill();
    }

    function getChunk(x, y) {
        let chunkX = Math.round(x / map.chunkSize);
        let chunkY = Math.round(y / map.chunkSize);

        let chunkPosition = chunkX + ', ' + chunkY;
        let chunk = cappedChunksMap.get(chunkPosition);

        if (chunk) {
            return chunk.value;
        }
    }

    function getFieldDescriptor(fieldValue) {
        var value = -0.5;

        for (i = 0; i < map.fieldDescriptors.length; i++) {

            var fieldDescriptor = map.fieldDescriptors[i];
            value += fieldDescriptor.range.to;

            if (value > fieldValue) {
                return fieldDescriptor;
            }
        }

        throw 'No matching field found for value ' + fieldValue + '.';
    }

    function getMoveDestination(startingPosition, step, moveDirectionName) {
        var moveFactor;

        switch (moveDirectionName) {
            case 'north':
                moveFactor = { X: 0, Y: 1 };
                break;
            case 'northEast':
                moveFactor = { X: 1, Y: 1 };
                break;
            case 'east':
                moveFactor = { X: 1, Y: 0 };
                break;
            case 'southEast':
                moveFactor = { X: 1, Y: -1 };
                break;
            case 'south':
                moveFactor = { X: 0, Y: -1 };
                break;
            case 'southWest':
                moveFactor = { X: -1, Y: -1 };
                break;
            case 'west':
                moveFactor = { X: -1, Y: 0 };
                break;
            case 'northWest':
                moveFactor = { X: -1, Y: 1 };
                break;
            default:
                throw 'Unknown move direction name.';
        }

        return {
            X: startingPosition.X + step * moveFactor.X,
            Y: startingPosition.Y + step * moveFactor.Y
        };
    }

    function getRandomColor() {
        return '#' + Math.floor(Math.random() * 16777215).toString(16);
    }

    function isInVisibleMapBounds(position) {
        var currentPlayerPosition = playersDict[currentPlayerName].position;
        let mapX = position.X;
        let mapY = position.Y;

        return !(Math.abs(currentPlayerPosition.X - mapX) > Math.floor(columns / 2)
            || Math.abs(currentPlayerPosition.Y - mapY) > Math.floor(rows / 2));
    }

    function isOccupiedByPlayer(x, y) {
        if (Object.keys(playersDict).length === 0) {
            return false;
        }

        for (var key in playersDict) {
            var player = playersDict[key];

            if (player.position.X === x && player.position.Y === y) {
                return true;
            }
        }

        return false;
    }

    function movePlayer(moveDirectionName) {
        let currentPlayer = playersDict[currentPlayerName];
        let currentPosition = currentPlayer.position;
        let pingKey = `${currentPosition.X}, ${currentPosition.Y}`;
        let destination = getMoveDestination(currentPosition, 1, moveDirectionName);

        if (!checkIfMoveIsValid(destination)) {
            return;
        }

        // Set new position in players dictionary.
        currentPlayer.position = {
            X: destination.X,
            Y: destination.Y
        };
        playersDict[currentPlayerName] = currentPlayer;

        clearCanvas();
        displayMapFields();
        displayPlayerFields();
        displayCurrentPlayerCoordinates();

        ping[pingKey] = performance.now();
        connection.invoke('movePlayer', moveDirectionName);
    }

    function loadMissingMapChunks(playerPosition) {
        let missingChunks = {};

        for (var canvasX = 0; canvasX < columns; canvasX++) {
            let mapX = playerPosition.X + canvasX - Math.floor(columns / 2);

            for (var canvasY = rows - 1; canvasY >= 0; canvasY--) {
                let mapY = playerPosition.Y + Math.floor(rows / 2) - canvasY;
                let chunk = getChunk(mapX, mapY);

                if (!chunk) {
                    let chunkX = Math.round(mapX / map.chunkSize);
                    let chunkY = Math.round(mapY / map.chunkSize);
                    let chunkPosition = chunkX + ', ' + chunkY;

                    missingChunks[chunkPosition] = { X: chunkX, y: chunkY };
                }

                mapY++;
            }

            mapX++;
        }

        if (Object.keys(missingChunks).length > 0) {
            connection.invoke('getMapChunks', Object.keys(missingChunks));
        }
    }

    function notifyMessage() {
        if (!chatVisible) {
            $('#chatMessageNotification').show();
            $('#chatMessageNotification').text(parseInt($('#chatMessageNotification').text()) + 1);
            $('#chatMessageNotification').addClass('bounce');
        }
    }

    function regenerateMap() {
        connection.invoke('regenerateMap');
    }

    function scrollToTheDiscussionBottom() {
        $('#discussionContainer')[0].scrollTop = $('#discussionContainer')[0].scrollHeight;
    };

    function sendMessage(event) {
        if ($messageInput.val().trim() === '') {
            event.preventDefault();
            return;
        }

        // Call the sendMessage method on the hub.
        connection.invoke('sendMessage', currentPlayerName, $messageInput.val());

        // Clear text box and reset focus for next comment.
        $messageInput.val('');
        $messageInput.focus();
        event.preventDefault();
    }

    function updatePlayerInfo(newPlayerName, newPlayerColor) {
        connection.invoke('updatePlayerInfo', newPlayerName, newPlayerColor);
    }

    function update() {
        var multiplier = 1.0;

        if (!keyboardEnabled) {
            keyMap = {};
            setTimeout(update, frameDelay * multiplier);
            return;
        }

        // North West
        if ((keyMap['ArrowUp'] && keyMap['ArrowLeft'])
            || (keyMap['w'] && keyMap['a'])) {
            setTimeout(() => movePlayer('northWest', 0));
            multiplier = Math.SQRT2 / 1;
        }

        // North East
        else if ((keyMap['ArrowUp'] && keyMap['ArrowRight'])
            || (keyMap['w'] && keyMap['d'])) {
            setTimeout(() => movePlayer('northEast', 0));
            multiplier = Math.SQRT2 / 1;
        }

        // South East
        else if ((keyMap['ArrowDown'] && keyMap['ArrowRight'])
            || (keyMap['s'] && keyMap['d'])) {
            setTimeout(() => movePlayer('southEast', 0));
            multiplier = Math.SQRT2 / 1;
        }

        // South West
        else if ((keyMap['ArrowDown'] && keyMap['ArrowLeft'])
            || (keyMap['s'] && keyMap['a'])) {
            setTimeout(() => movePlayer('southWest', 0));
            multiplier = Math.SQRT2 / 1;
        }

        // North
        else if (keyMap['ArrowUp'] || keyMap['w']) {
            setTimeout(() => movePlayer('north', 0));
        }

        // East
        else if (keyMap['ArrowRight'] || keyMap['d']) {
            setTimeout(() => movePlayer('east', 0));
        }

        // South
        else if (keyMap['ArrowDown'] || keyMap['s']) {
            setTimeout(() => movePlayer('south', 0));
        }

        // West
        else if (keyMap['ArrowLeft'] || keyMap['a']) {
            setTimeout(() => movePlayer('west', 0));
        }

        setTimeout(update, frameDelay * multiplier);
    }

    update();
});