﻿class CappedMap {
    constructor(cap) {
        if (!Number.isInteger(cap)) {
            throw 'cap must be a number!';
        }

        var _map = {};
        var _cap = cap;
        var _count = 0;
        var _firstEntry = null;
        var _lastEntry = null;

        this.getFirstEntry = () => _firstEntry;
        this.getLastEntry = () => _lastEntry;

        this.getCap = () => _cap;

        this.getCount = () => _count;

        this.add = (key, value) => {
            if (key in _map) {
                _moveToTheEnd(key);
            } else {
                if (_count === 0) {
                    _firstEntry = {
                        key: key,
                        value: value,
                    }
                    _lastEntry = _firstEntry;
                    _map[key] = _firstEntry;
                } else {
                    var newEntry = {
                        key: key,
                        value: value,
                        previous: _lastEntry,
                    };

                    _lastEntry['next'] = newEntry;
                    _map[_lastEntry['key']] = _lastEntry;

                    _lastEntry = newEntry;
                    _map[key] = newEntry;
                }

                _count++;
                _trim();
            }
        };

        this.contains = (key) => {
            if (key in _map) {
                _moveToTheEnd(key);

                return true;
            }

            return false;
        };

        this.get = (key) => {
            var value = _map[key];

            if (value) {
                _moveToTheEnd(key);

                return value;
            }
        };

        this.remove = (key) => {
            if (key in _map) {
                var entryToRemove = _map[key];
                _count--;

                _zip(entryToRemove, entryToRemove['previous'], entryToRemove['next']);

                delete _map[key];
            }
        };

        // Private functions
        var _moveToTheEnd = (key) => {
            if (this.getCount() === 1) {
                return;
            }

            var entryToMove = _map[key];

            _zip(entryToMove['previous'], entryToMove['next']);

            delete entryToMove['next'];
            _lastEntry['next'] = entryToMove;
            entryToMove['previous'] = _lastEntry;
            _lastEntry = entryToMove;
        };

        var _trim = () => {
            if (_count > this.getCap()) {
                _count--;
                delete _map[_firstEntry['key']];
                var newFirstEntry = _firstEntry['next'];
                delete newFirstEntry['previous'];
                _firstEntry = newFirstEntry;
            }
        };

        var _zip = (previous, next) => {
            if (!previous && !next) {
                return;
            }

            if (previous) {
                previous['next'] = next;
            } else {
                _firstEntry = _firstEntry['next'];
            }

            if (next) {
                next['previous'] = previous;
            } else {
                _lastEntry = _lastEntry['previous'];
            }
        };
    }
}