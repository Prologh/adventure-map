﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AdventureMap.WebApp.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View(new MapViewModel
            {
                Columns = Map.DefaultStartingColumns,
                FieldSize = Map.DefaultFieldSize,
                Rows = Map.DefaultStartingRows,
            });
        }
    }
}
