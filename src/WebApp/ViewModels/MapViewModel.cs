﻿using System.ComponentModel.DataAnnotations;

namespace AdventureMap.WebApp.ViewModels
{
    public class MapViewModel
    {
        public MapViewModel()
        {

        }

        [Range(1, 500)]
        public int Columns { get; set; }

        [Range(1, 25)]
        public int FieldSize { get; set; }

        [Range(1, 500)]
        public int Rows { get; set; }
    }
}
