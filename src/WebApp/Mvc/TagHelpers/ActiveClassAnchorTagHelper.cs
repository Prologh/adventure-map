﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace AdventureMap.WebApp.Mvc.TagHelpers
{
    [HtmlTargetElement("a", Attributes = "asp-add-active-class")]
    public class ActiveClassAnchorTagHelper : TagHelper
    {
        private const string DefaultActiveClassName = "active";

        [HtmlAttributeName("asp-active-class-name")]
        public string ActiveClassName { get; set; }

        [HtmlAttributeName("asp-add-active-class")]
        public bool AddActiveClass { get; set; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (!AddActiveClass)
            {
                return;
            }

            // Get route data from context.
            var routeArea = GetRouteArea();
            var routeController = GetRouteController();

            // Get route data from anchor tag attributes.
            var anchorArea = GetAttributeValue(context, "asp-area");
            var anchorController = GetAttributeValue(context, "asp-controller");

            if (routeArea == anchorArea && routeController == anchorController)
            {
                AddCssClassToOutput(output);
            }
        }

        private void AddCssClassToOutput(TagHelperOutput output)
        {
            var activeCssClassToAdd = ActiveClassName ?? DefaultActiveClassName;

            if (output.Attributes.TryGetAttribute("class", out var cssClassAttribute))
            {
                var existingCssClassValue = cssClassAttribute.Value?.ToString();

                output.Attributes.SetAttribute(
                    "class",
                    $"{existingCssClassValue} {activeCssClassToAdd}");
            }
            else
            {
                output.Attributes.SetAttribute("class", activeCssClassToAdd);
            }
        }

        private string GetAttributeValue(TagHelperContext context, string attributeName)
        {
            if (context.AllAttributes.TryGetAttribute(
                attributeName,
                out var tagHelperAttribute))
            {
                return tagHelperAttribute.Value?.ToString() ?? string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetRouteArea()
        {
            return ViewContext.RouteData.Values["area"]?.ToString()
                ?? string.Empty;
        }

        private string GetRouteController()
        {
            return ViewContext.RouteData.Values["controller"]?.ToString()
                ?? string.Empty;
        }
    }
}
