﻿using AdventureMap.WebApp.Filters;
using AdventureMap.WebApp.Hubs;
using AdventureMap.WebApp.Options;
using AdventureMap.WebApp.Services.Chunks;
using AdventureMap.WebApp.Services.Fields;
using AdventureMap.WebApp.Services.Maps;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace AdventureMap.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc(options => options.Filters.Add<InvalidModelFilter>())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpsRedirection(options =>
            {
                options.HttpsPort = 443;
            });

            services.AddSignalR();

            services.AddSingleton<IFieldDescriptorProvider, DefaultFieldDescriptorProvider>();
            services.AddSingleton<IMapManager, InMemorySingleMapManager>();
            services.AddSingleton<IChunkStore, CacheChunkStore>();

            services.AddTransient<IFieldWalkabilityValidator, DefaultFieldWalkabilityValidator>();
            //services.AddTransient<IMapWalkablilityCalculator, DefaultMapWalkabilityCalculator>();
            services.AddTransient<IMapGenerator, NoiseMapGenerator>();
            services.AddTransient<IMoveDirectionProvider, DefaultMoveDirectionProvider>();
            services.AddTransient<IMoveDestinationProvider, DefaultMoveDestinationProvider>();
            services.AddTransient<IMoveValidator, DefaultMoveValidator>();
            services.AddTransient<IStartingPositionProvider, RandomStartingPostionProvider>();

            services.Configure<MapOptions>(Configuration.GetSection("Map"));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSignalR(routes =>
            {
                routes.MapHub<MapHub>("/hubs/map");
            });
            app.UseMvcWithDefaultRoute();
        }
    }
}
