﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AdventureMap.WebApp.Models
{
    public class Map
    {
        public const int DefaultChunkSideLength = 15;
        public const int DefaultFieldSize = 20;
        public const int DefaultResolutionMultiplayer = 4;
        public const int DefaultStartingColumns = 16 * DefaultResolutionMultiplayer;
        public const int DefaultStartingRows = 9 * DefaultResolutionMultiplayer;

        public Map(int seed, int columns, int rows, int chunkSize)
        {
            ChunkSize = chunkSize;
            FieldDescriptors = new List<FieldDescriptor>();
            Players = new Dictionary<string, Player>();
            Seed = seed;
            SpawnSize = new Size(columns, rows);
        }

        public Map(int seed, int columns, int rows, int chunkSize, IReadOnlyList<FieldDescriptor> fieldDescriptors)
            : this(seed, columns, rows, chunkSize)
        {
            FieldDescriptors = fieldDescriptors ?? throw new ArgumentNullException(nameof(fieldDescriptors));
        }

        public int ChunkSize { get; }

        public IReadOnlyList<FieldDescriptor> FieldDescriptors { get; }

        public IDictionary<string, Player> Players { get; }

        public int Seed { get; }

        [JsonConverter(typeof(Json.Converters.SizeConverter))]
        public Size SpawnSize { get; }

        public void AddPlayer(Player newPlayer, string key)
        {
            if (newPlayer is null)
            {
                throw new ArgumentNullException(nameof(newPlayer));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Key cannot be null, empty or whitespace.", nameof(key));
            }

            if (Players.Values.Any(player => player.Position == new Point(newPlayer.Position.X, newPlayer.Position.Y)))
            {
                throw new InvalidOperationException(
                    "Cannot assign a player to a field already occupied by other player.");
            }

            Players[key] = newPlayer;
        }

        //public bool CheckIfInBounds(int fieldX, int fieldY)
        //{
        //    var chunkPosition = new Point(
        //        (int)Math.Round(fieldX * 1.0d / ChunkSize),
        //        (int)Math.Round(fieldY * 1.0d / ChunkSize));

        //    return Chunks.ContainsKey(chunkPosition);
        //}

        //public Chunk GetChunkByFieldPosition(int fieldX, int fieldY)
        //{
        //    return Chunks[new Point(
        //        (int)Math.Round(fieldX * 1.0d / ChunkSize),
        //        (int)Math.Round(fieldY * 1.0d / ChunkSize))];
        //}

        //public double GetFieldValueByFieldPosition(int fieldX, int fieldY)
        //{
        //    var chunk = GetChunkByFieldPosition(fieldX, fieldY);

        //    var localX = fieldX + -chunk.Position.X * ChunkSize + ChunkSize / 2;
        //    var localY = fieldY + -chunk.Position.Y * ChunkSize + ChunkSize / 2;

        //    return chunk.Fields[localX, localY];
        //}

        public void MovePlayer(Player playerToMove, Point destination)
        {
            if (playerToMove is null)
            {
                throw new ArgumentNullException(nameof(playerToMove));
            }

            if (destination == playerToMove.Position)
            {
                throw new InvalidOperationException("Cannot move a player to the same position");
            }

            if (Players.Values.Any(player => player.Position == destination))
            {
                throw new InvalidOperationException(
                    "Cannot move a player to a field already occupied by other player.");
            }

            playerToMove.Position = destination;
        }

        public void RemovePlayer(string name) => Players.Remove(name);
    }
}
