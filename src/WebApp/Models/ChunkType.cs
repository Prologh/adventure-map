﻿namespace AdventureMap.WebApp.Models
{
    public enum ChunkType
    {
        Spawn = 0,
        Remote = 1,
    }
}
