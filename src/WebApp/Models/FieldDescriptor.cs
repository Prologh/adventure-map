﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Drawing;

namespace AdventureMap.WebApp.Models
{
    [DebuggerDisplay("{Name} {Range}")]
    public class FieldDescriptor
    {
        [JsonConverter(typeof(Json.Converters.ColorConverter))]
        public Color Color { get; set; }

        public IconInfo IconInfo { get; set; }

        public bool IsWalkable { get; set; }

        public string Name { get; set; }

        public Range<double> Range { get; set; }
    }
}
