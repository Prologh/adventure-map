﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Drawing;

namespace AdventureMap.WebApp.Models
{
    [DebuggerDisplay("{Name} at {Position}")]
    public class Player
    {
        public Player()
        {

        }

        [JsonConverter(typeof(Json.Converters.ColorConverter))]
        public Color Color { get; set; }

        public string Name { get; set; }

        [JsonConverter(typeof(Json.Converters.PointConverter))]
        public Point Position { get; set; }
    }
}
