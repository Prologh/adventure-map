﻿namespace AdventureMap.WebApp.Models
{
    public enum WorldQuarter
    {
        Center,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest,
    }
}
