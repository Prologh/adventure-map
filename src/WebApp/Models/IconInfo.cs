﻿using System.Diagnostics;

namespace AdventureMap.WebApp.Models
{
    [DebuggerDisplay("{Width}x{Height} at {FilePath}")]
    public class IconInfo
    {
        public string FilePath { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }
    }
}
