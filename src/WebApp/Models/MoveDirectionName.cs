﻿namespace AdventureMap.WebApp.Models
{
    public enum MoveDirectionName
    {
        North = 0,
        NorthWest,
        West,
        SouthWest,
        South,
        SouthEast,
        East,
        NorthEast,
    }
}
