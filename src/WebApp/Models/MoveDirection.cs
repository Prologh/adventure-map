﻿using System.Diagnostics;
using System.Drawing;

namespace AdventureMap.WebApp.Models
{
    [DebuggerDisplay("{Name,nq}")]
    public sealed class MoveDirection
    {
        public static MoveDirection East => new MoveDirection(nameof(East), x: 1, y: 0);
        public static MoveDirection North => new MoveDirection(nameof(North), x: 0, y: 1);
        public static MoveDirection NorthEast => new MoveDirection(nameof(NorthEast), x: 1, y: 1);
        public static MoveDirection NorthWest => new MoveDirection(nameof(NorthWest), x: -1, y: 1);
        public static MoveDirection South => new MoveDirection(nameof(South), x: 0, y: -1);
        public static MoveDirection SouthEast => new MoveDirection(nameof(SouthEast), x: 1, y: -1);
        public static MoveDirection SouthWest => new MoveDirection(nameof(SouthWest), x: -1, y: -1);
        public static MoveDirection West => new MoveDirection(nameof(WebApp), x: -1, y: 0);

        private MoveDirection(string name, int x, int y)
        {
            Name = name;
            Vector = new Size(x, y);
        }

        public string Name { get; }

        public Size Vector { get; }
    }
}
