﻿using System;

namespace AdventureMap.WebApp.Models
{
    public readonly struct Range<T> : IEquatable<Range<T>> where T : struct
    {
        public Range(T from, T to)
        {
            From = from;
            To = to;
        }

        public T From { get; }

        public T To { get; }

        public override bool Equals(object obj)
        {
            return obj is Range<T> range
                ? obj.Equals(range)
                : Equals(obj, this);
        }

        public bool Equals(Range<T> other)
        {
            return other.From.Equals(From)
                && other.To.Equals(To);
        }

        public override int GetHashCode()
        {
            return From.GetHashCode() * To.GetHashCode();
        }

        public override string ToString()
        {
            return $"{nameof(From)} {From} {nameof(To)} {To}";
        }
    }
}
