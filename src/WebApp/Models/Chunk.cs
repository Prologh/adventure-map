﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Drawing;

namespace AdventureMap.WebApp.Models
{
    [DebuggerDisplay("{Size}x{Size} at [{Position.X}, {Position.Y}] in {GetWorldQuarter()}")]
    public readonly struct Chunk
    {
        public Chunk(int size, Point position, ChunkType type, double[,] fields)
        {
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(size),
                    $"{nameof(size)} of a chunk cannot be equal to or less than zero.");
            }

            if (fields.GetLength(0) != size || fields.GetLength(1) != size)
            {
                throw new ArgumentException(
                        $"The size of a {nameof(fields)} array have to be " +
                        $"a two dimensional array with length matching size of a chunk.",
                    nameof(fields));
            }

            Fields = fields;
            Position = position;
            Size = size;
            Type = type;
        }

        public double [,] Fields { get; }

        [JsonConverter(typeof(Json.Converters.PointConverter))]
        public Point Position { get; }

        public int Size { get; }

        public ChunkType Type{ get; }

        public WorldQuarter GetWorldQuarter()
        {
            if (Position.X > 0)
            {
                if (Position.Y > 0)
                {
                    return WorldQuarter.NorthEast;
                }
                else
                {
                    if (Position.Y == 0)
                    {
                        return WorldQuarter.East;
                    }
                    else
                    {
                        return WorldQuarter.SouthEast;
                    }
                }
            }
            else
            {
                if (Position.X == 0)
                {
                    if (Position.Y > 0)
                    {
                        return WorldQuarter.North;
                    }
                    else
                    {
                        if (Position.Y == 0)
                        {
                            return WorldQuarter.Center;
                        }
                        else
                        {
                            return WorldQuarter.South;
                        }
                    }
                }
                else
                {
                    if (Position.Y > 0)
                    {
                        return WorldQuarter.NorthWest;
                    }
                    else
                    {
                        if (Position.Y == 0)
                        {
                            return WorldQuarter.West;
                        }
                        else
                        {
                            return WorldQuarter.SouthWest;
                        }
                    }
                }
            }
        }
    }
}
