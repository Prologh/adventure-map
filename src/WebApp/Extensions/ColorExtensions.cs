﻿using System.Drawing;

namespace AdventureMap.Extensions
{
    public static class ColorExtensions
    {
        public static string ToHex(this Color color)
        {
            return "#" +
                color.R.ToString("X2") +
                color.G.ToString("X2") +
                color.B.ToString("X2");
        }

        public static string ToRgb(this Color color)
        {
            return "rgb(" +
                color.R.ToString() + "," +
                color.G.ToString() + "," +
                color.B.ToString() + ")";
        }

        public static string ToArgb(this Color color)
        {
            return "rgb(" +
                color.R.ToString() + "," +
                color.G.ToString() + "," +
                color.B.ToString() + "," +
                color.A.ToString() + ")";
        }
    }
}
