﻿using Newtonsoft.Json;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Json.Converters
{
    public class PointConverter : JsonConverter<Point>
    {
        public override bool CanRead => false;

        public override bool CanWrite => true;

        public override void WriteJson(JsonWriter writer, Point value, JsonSerializer serializer)
        {
            writer.WriteStartObject();

            writer.WritePropertyName(nameof(Point.X));
            serializer.Serialize(writer, value.X);

            writer.WritePropertyName(nameof(Point.Y));
            serializer.Serialize(writer, value.Y);

            writer.WriteEndObject();
        }

        public override Point ReadJson(JsonReader reader, Type objectType, Point existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
