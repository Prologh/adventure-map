﻿using Newtonsoft.Json;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Json.Converters
{
    public class ColorConverter : JsonConverter<Color>
    {
        public override bool CanRead => false;

        public override bool CanWrite => true;

        public override void WriteJson(JsonWriter writer, Color value, JsonSerializer serializer)
        {
            var hexColor = $"#{value.R.ToString("X2")}{value.G.ToString("X2")}{value.B.ToString("X2")}";

            writer.WriteValue(hexColor);
        }

        public override Color ReadJson(JsonReader reader, Type objectType, Color existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }
    }
}
