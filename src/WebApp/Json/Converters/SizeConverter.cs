﻿using Newtonsoft.Json;
using System;
using System.Drawing;

namespace AdventureMap.WebApp.Json.Converters
{
    public class SizeConverter : JsonConverter<Size>
    {
        public override bool CanRead => false;

        public override bool CanWrite => true;

        public override void WriteJson(JsonWriter writer, Size value, JsonSerializer serializer)
        {
            writer.WriteStartObject();

            writer.WritePropertyName(nameof(Size.Width));
            serializer.Serialize(writer, value.Width);

            writer.WritePropertyName(nameof(Size.Height));
            serializer.Serialize(writer, value.Height);

            writer.WriteEndObject();
        }

        public override Size ReadJson(JsonReader reader, Type objectType, Size existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
