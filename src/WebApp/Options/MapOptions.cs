﻿using System;

namespace AdventureMap.WebApp.Options
{
    public class MapOptions
    {
        public TimeSpan ChunkExpirationTime { get; set; }
    }
}
