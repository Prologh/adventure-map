﻿using AdventureMap.WebApp.Models;
using AdventureMap.WebApp.Services.Fields;
using AdventureMap.WebApp.Services.Maps;
using AdventureMap.WebApp.ViewModels;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureMap.WebApp.Hubs
{
    public class MapHub : Hub
    {
        private readonly IMapManager _mapManager;
        private readonly IMoveDestinationProvider _moveDestinationProvider;
        private readonly IStartingPositionProvider _startingPositionProvider;
        private readonly ILogger<MapHub> _logger;

        public MapHub(
            IMapManager mapManager,
            IMoveDestinationProvider moveDestinationProvider,
            IStartingPositionProvider startingPositionProvider,
            ILogger<MapHub> logger)
        {
            _mapManager = mapManager;
            _moveDestinationProvider = moveDestinationProvider;
            _startingPositionProvider = startingPositionProvider;
            _logger = logger;
        }

        public async override Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);

            if (_mapManager.CurrentMap?.Players.Any() ?? false)
            {
                await RemovePlayer();
            }
        }

        public async Task AddPlayer(string name, [JsonConverter(typeof(Json.Converters.ColorConverter))] Color playerColor)
        {
            var newPlayer = new Player
            {
                Color = playerColor,
                Name = name,
                Position = _startingPositionProvider.GetStartingPosition(_mapManager.CurrentMap),
            };
            _mapManager.CurrentMap.AddPlayer(newPlayer, Context.ConnectionId);

            await Clients.All.SendAsync("addPlayer", newPlayer);
        }

        public async Task GetMapChunks(Point[] chunkPositions)
        {
            var chunks = new Chunk[chunkPositions.Length];

            for (int i = 0; i < chunkPositions.Length; i++)
            {
                var chunkPosition = chunkPositions[i];

                if (!_mapManager.TryGetMapChunk(chunkPosition, out var chunk))
                {
                    chunk = _mapManager.GenerateMapChunk(chunkPosition);
                }

                chunks[i] = chunk;
            }

            await Clients.Caller.SendAsync("getMapChunks", chunks);
        }

        public async Task GetMapInfo(int columns, int rows)
        {
            if (!_mapManager.HasGeneratedMap)
            {
                _mapManager.GenerateMap(columns, rows);
            }

            var map = _mapManager.CurrentMap;

            await Clients.Caller.SendAsync("getMapInfo", map);
        }

        public async Task MovePlayer(MoveDirectionName moveDirectionName)
        {
            var playerToMove = _mapManager.CurrentMap.Players[Context.ConnectionId];
            var startingPosition = playerToMove.Position;
            var destination = _moveDestinationProvider.GetMoveDestination(startingPosition, step: 1, moveDirectionName);

            _mapManager.CurrentMap.MovePlayer(playerToMove, destination);

            await Clients.All.SendAsync("movePlayer", playerToMove.Name, startingPosition.X, startingPosition.Y, destination.X, destination.Y);
        }

        public async Task SendMessage(string author, string message)
        {
            await Clients.All.SendAsync("sendMessage", author, message);
        }

        public async Task RegenerateMap()
        {
            _logger.LogInformation("Map regeneration requested by user.");

            _mapManager.RegenerateMap();

            _logger.LogInformation("Sending map regeneration request to all clients.");

            await Clients.All.SendAsync("regenerateMap");

            _logger.LogInformation("Map regeneration requests sent.");
        }

        public async Task RemovePlayer()
        {
            await Clients.All.SendAsync("removePlayer", _mapManager.CurrentMap.Players[Context.ConnectionId]);

            _mapManager.CurrentMap.RemovePlayer(Context.ConnectionId);
        }

        public async Task UpdatePlayerInfo(string playerName, [JsonConverter(typeof(Json.Converters.ColorConverter))] Color playerColor)
        {
            var player = _mapManager.CurrentMap.Players[Context.ConnectionId];
            var oldPlayerName = player.Name;
            player.Name = playerName;
            player.Color = playerColor;
            _mapManager.CurrentMap.Players[Context.ConnectionId] = player;

            await Clients.All.SendAsync("updatePlayerInfo", oldPlayerName, player);
        }
    }
}
